import pandas as pd
import numpy as np


from sklearn.metrics import accuracy_score



class metrics(object):
	
	"""
	This class contains metrics for checking predictions
	made by machine learning algorithms	
	"""

	def __init__(self):
		pass


	def accuracy(self, y_true, y_pred):
		"""
		args:
			y_true: true labels
			y_pred: predicted labels
		returns: 
			accuracy score
		"""

		return accuracy_score(y_true,y_pred)




	def __del__(self):
		pass
